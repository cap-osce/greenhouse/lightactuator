#include <Arduino.h>
#include <FastLED.h>
#include "SSD1306Wire.h"
#include "AWSParameters.h"
#include "IOTGateway.h"
#include "StandardConfig.h"
#include <ArduinoJson.h>

#define NUM_STRIPS 1
#define NUM_LEDS_PER_STRIP 147
CRGB leds[NUM_STRIPS][NUM_LEDS_PER_STRIP];

SSD1306Wire  display(0x3c, 5, 4);
IOTGateway iotGateway(&capgeminiHackathon);

int msgReceived = 0;
String payload;
String rcvdPayload;
int latest;

bool change = true;
bool lightsOn = false;

void callBackHandler (char *topicName, int payloadLen, char *payLoad) {
  char rcvdPayloadChar[512];

  strncpy(rcvdPayloadChar, payLoad, payloadLen);
  rcvdPayloadChar[payloadLen] = 0;
  rcvdPayload = rcvdPayloadChar;

  StaticJsonDocument<512> message;
  DeserializationError err = deserializeJson(message, rcvdPayload);

  if (err) {
    Serial.print(F("deserializeJson() failed with code "));
    Serial.println(err.c_str());
  } else {
    latest = message["value"];
    msgReceived = 1;
  }
}
 
void receiveMessage() {
  if(msgReceived == 1) {
    msgReceived = 0;
    change = true;
    Serial.print("Received: ");
    Serial.println(latest);
  }
}

void setup() {
  FastLED.addLeds<NEOPIXEL, 12>(leds[0], NUM_LEDS_PER_STRIP);
  Serial.begin(115200);
  
  // Initialising the UI will init the display too.
  display.init();
  display.flipScreenVertically();
  display.clear();
  Serial.println("Setup complete");

  iotGateway.initialise();
  display.clear();
  display.drawString(0,0,"Subscribe");
  display.display();

  delay(2500);
  iotGateway.subscribe("command/light", callBackHandler);
  display.clear();

  delay(2500);
}

void setLights(const CRGB colour, int brightnessPercent)
{
  for(int x = 0; x < NUM_STRIPS; x++) {
    for(int i = 0; i < NUM_LEDS_PER_STRIP; i++) {     
      leds[x][i] = colour;
      FastLED.setBrightness(map(brightnessPercent,0,100, 0, 102));
      FastLED.show();
    }
  }
}

void loop() {
  receiveMessage();

  if (change)
  {
    setLights(CRGB::White, latest);
    change = false;
  }

  delay(500);
}